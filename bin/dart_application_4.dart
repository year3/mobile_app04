import 'dart:io';

void Enterlottery() {
  print("Enter Lottery Number");
}

void checkall(var number) {
  if (number.length != 6) {
    print("Invalid number");
    return;
  }

  if (number == "436594") {
    print("You win the first prize 6,000,000 Baht");
    return;
  }
}

void checkfrontnum(var number) {
  if (number.substring(0, 3) == "266" || number.substring(0, 3) == "893") {
    print("You win the front number prize 4,000 Baht");
    return;
  }
}

void checkbacknum(var number) {
  if (number.substring(number.length - 3) == "282" ||
      number.substring(number.length - 3) == "447") {
    print("You win the back number prize 4,000 Baht");
    return;
  }
}

void checktwobacknum(var number) {
  if (number.substring(number.length - 2) == "14") {
    print("You win the two back number prize 2,000 Baht");
    return;
  } else {
    print("ํYou are unlucky");
  }
}

void checklottery(var number) {
  checkall(number);
  checkfrontnum(number);
  checkbacknum(number);
  checktwobacknum(number);
}

void main() {
  Enterlottery();
  var number = stdin.readLineSync();
  checklottery(number);
}
